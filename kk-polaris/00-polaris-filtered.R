# load packages 
library(readr)
library(dplyr)
library(stringr)
library(lubridate)

## loading data ####
polar <- read_csv('GW-FINAL-no-outliers-traders-only.csv')
#polar <- read_csv('GW_FINAL-no-outliers.csv')
#polar <- read_csv('GW_FINAL-with-outliers.csv')
str(polar)
glimpse(polar)


#### data cleaning ####
new_polar <- polar %>% 
  select(1:14, contains("technical"), contains("current")) %>% 
  mutate(policy_start = dmy(policy_start),
         policy_end =  dmy(policy_end),
         renewal_date = dmy(renewal_date),
         policy_type = str_sub(policy_number, -3, -1)
  ) 


# sense check 
head(new_polar)
dim(new_polar)
summary(new_polar$policy_start)
table(new_polar$policy_type)

n_distinct(new_polar$policy_number)
unique(new_polar$product_code)


#### save the final dataset as CSV 
write_csv(new_polar, "20181001-no-outliers-polaris-traders-only.csv")
#write_csv(new_polar, "20181001-no-outliers-polaris.csv")
#write_csv(new_polar, "20181001-with-outliers-polaris.csv")
