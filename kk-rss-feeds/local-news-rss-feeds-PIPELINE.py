
# coding: utf-8

# In[13]:

# import packages 
import pandas as pd
import requests 
import datetime
#from memento_client import MementoClient
#import urllib2
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import os
import io
import feedparser
import json
from sqlalchemy import create_engine


# ## BBC LOCAL NEWS BRANCHES

# In[14]:

area  = ['Cumbria',
            'Lancashire',
            'Liverpool',
            'Manchester',
            'Tees',
            'Tyne & Wear',
            'Humberside',
            'Leeds & West Yorkshire',
            'Lincolnshire',
            'Sheffield & South Yorkshire',
            'York & North Yorkshire',
            'Birmingham & Black Country',
            'Coventry & Warwickshire',
            'Hereford & Worcester',
            'Shropshire',
            'Stoke & Staffordshire',
            'Derby',
            'Leicester',
            'Northampton',
            'Nottingham',
            'Bristol',
            'Cornwall',
            'Devon',
            'Gloucestershire',
            'Somerset',
            'Wiltshire',
            'Beds, Herts & Bucks',
            'Cambridgeshire',
            'Essex',
            'Norfolk',
            'Suffolk',
            'Berkshire',
            'Dorset',
            'Hampshire & Isle of Wight',
            'Oxford',
            'Kent',
            'London',
            'Surrey',
            'Sussex',
            'Isle of Man/Ellan Vannin',
            'Guernsey',
            'Jersey']

bbc_link = ["https://www.bbc.co.uk/news/england/cumbria",                     
"https://www.bbc.co.uk/news/england/lancashire",                  
"https://www.bbc.co.uk/news/england/liverpool",                   
"https://www.bbc.co.uk/news/england/manchester",                  
"https://www.bbc.co.uk/news/england/tees",                        
"https://www.bbc.co.uk/news/england/tyne_and_wear",               
"https://www.bbc.co.uk/news/england/humberside",                  
"https://www.bbc.co.uk/news/england/leeds_and_west_yorkshire",    
"https://www.bbc.co.uk/news/england/lincolnshire",                
"https://www.bbc.co.uk/news/england/south_yorkshire",             
"https://www.bbc.co.uk/news/england/york_and_north_yorkshire",    
"https://www.bbc.co.uk/news/england/birmingham_and_black_country",
"https://www.bbc.co.uk/news/england/coventry_and_warwickshire",   
"https://www.bbc.co.uk/news/england/hereford_and_worcester",      
"https://www.bbc.co.uk/news/england/shropshire",                  
"https://www.bbc.co.uk/news/england/stoke_and_staffordshire",     
"https://www.bbc.co.uk/news/england/derby",                       
"https://www.bbc.co.uk/news/england/leicester",                   
"https://www.bbc.co.uk/news/england/northampton",                 
"https://www.bbc.co.uk/news/england/nottingham",                  
"https://www.bbc.co.uk/news/england/bristol",                     
"https://www.bbc.co.uk/news/england/cornwall",                    
"https://www.bbc.co.uk/news/england/devon",                       
"https://www.bbc.co.uk/news/england/gloucestershire",             
"https://www.bbc.co.uk/news/england/somerset",                    
"https://www.bbc.co.uk/news/england/wiltshire",                   
"https://www.bbc.co.uk/news/england/beds_bucks_and_herts",        
"https://www.bbc.co.uk/news/england/cambridgeshire",              
"https://www.bbc.co.uk/news/england/essex",                       
"https://www.bbc.co.uk/news/england/norfolk",                     
"https://www.bbc.co.uk/news/england/suffolk",                     
"https://www.bbc.co.uk/news/england/berkshire",                   
"https://www.bbc.co.uk/news/england/dorset",                      
"https://www.bbc.co.uk/news/england/hampshire",                   
"https://www.bbc.co.uk/news/england/oxford",                      
"https://www.bbc.co.uk/news/england/kent",                        
"https://www.bbc.co.uk/news/england/london",                      
"https://www.bbc.co.uk/news/england/surrey",                      
"https://www.bbc.co.uk/news/england/sussex",                      
"https://www.bbc.co.uk/news/world/europe/isle_of_man",            
"https://www.bbc.co.uk/news/england/guernsey",                    
"https://www.bbc.co.uk/news/england/jersey"]

bbc_dict = {'area': area,
            'bbc_link': bbc_link
           }
bbc_df = pd.DataFrame(data = bbc_dict)
bbc_df


# In[15]:

# function to extract the word after the last slash (localtion)
def extract_name(url):
    result = url.rsplit('/', 1)[-1]
    return result


# In[16]:

# extract localtion and use it to create the final BBC RSS links
bbc_df['location'] = bbc_df['bbc_link'].apply(extract_name)
bbc_df['rss_link'] = 'http://feeds.bbci.co.uk/news/england/' + bbc_df['location'].map(str) + '/rss.xml'
bbc_df


# ## Connect to MySQL

# In[17]:

### define MySQL parameters
#Details required to connect to DB
host="nightingaledb.cjhl1bmdwdwk.us-east-1.rds.amazonaws.com"
port=3306
dbname="rss_feeds"
user="flozza"
password="zArp.9Fe#i"


# In[18]:

### connect to MySQL
connect_string = 'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4'.format(user,password,host,port,dbname)
engine = create_engine(connect_string, convert_unicode=True, echo=False)


# In[19]:

# view all available tables
sql_tbls = pd.read_sql('SELECT * FROM information_schema.tables', con = engine)
sql_tbls[sql_tbls['TABLE_SCHEMA'] == 'rss_feeds']


# ## getting archived RSS feeds URLs

# In[20]:

### getting archives
def get_archives(url):
    api_start = 'http://archive.org/wayback/available?url='
    cdx_uri = 'http://web.archive.org/cdx/search/cdx?url=' + url
    resp = get(cdx_uri)
    text = resp.text
    text = text.splitlines()
    return text

# split received text
def split_txt(text):
    split_items=[]
    for i in text:
        split_items.append(i.split(' '))
    return split_items

# save split_text as dataframe
def rss_to_df(split_text):
    df_test = pd.DataFrame(split_text)
    df_test.columns = ['rss_link', 'date', 'orig_link', 'format', 'status_code', 'code', 'number']
    df_test['final_link'] = 'http://web.archive.org/web/'+ df_test['date'].map(str) + '/' + df_test['orig_link'].map(str)
    return df_test


# # save archive links to MySQL

# In[22]:

### WRAP THIS UP INTO THE LOOP THAT WILL SAVE THE RESUTS TO MYSQL

#url = bbc_df['rss_link'][0]
for url in bbc_df['rss_link']:
    text = get_archives(url)
    if len(text) == 0:
        print("Empty archive at %s" %url)
    else:
        spl_text = split_txt(text)
        text_df = rss_to_df(spl_text)
        text_df.to_sql(con=engine, name='archived_rss_urls', if_exists='append',index=False)

## below news branches don't have any internet archives!!


# In[23]:

# check if it saved it on MySQL - looks ok
sql_test = pd.read_sql('SELECT * FROM  archived_rss_urls', con=engine)
sql_test.shape


# In[24]:

# check the sql table
sql_test.head()


# # scrape the content of archived RSS feeds and save it in MySQL

# In[25]:

# pull articles from a single rss feed 
### !!! needs adding location of the bbc branch to posts!!!! ###
def get_rss(url):
    posts = []
    feed = feedparser.parse(url)
    for post in feed.entries:
        posts.append((post.title, post.link, post.summary, post.published))
    return posts


# In[26]:

archive_link = sql_test.final_link#.tolist
#archive_link

for url in archive_link:
    articles = get_rss(url)
    articles_df = pd.DataFrame(articles, columns=['title', 'link', 'summary', 'date'])
    articles_df.to_sql(con=engine, name='archived_rss_data', if_exists='append',index=False)


# In[ ]:

# check if it saved it on MySQL - looks ok
sql_test2 = pd.read_sql('SELECT * FROM  archived_rss_data', con=engine)
sql_test2.shape


# In[ ]:

sql_test2.head()


# In[ ]:



