
# coding: utf-8

# In[1]:

# import packages 
import pandas as pd
#from memento_client import MementoClient
#import urllib2
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import os, io, feedparser, json, datetime, requests, feather

from sqlalchemy import create_engine


# ## BBC LOCAL NEWS BRANCHES

# In[2]:

area  = ['Cumbria',
            'Lancashire',
            'Liverpool',
            'Manchester',
            'Tees',
            'Tyne & Wear',
            'Humberside',
            'Leeds & West Yorkshire',
            'Lincolnshire',
            'Sheffield & South Yorkshire',
            'York & North Yorkshire',
            'Birmingham & Black Country',
            'Coventry & Warwickshire',
            'Hereford & Worcester',
            'Shropshire',
            'Stoke & Staffordshire',
            'Derby',
            'Leicester',
            'Northampton',
            'Nottingham',
            'Bristol',
            'Cornwall',
            'Devon',
            'Gloucestershire',
            'Somerset',
            'Wiltshire',
            'Beds, Herts & Bucks',
            'Cambridgeshire',
            'Essex',
            'Norfolk',
            'Suffolk',
            'Berkshire',
            'Dorset',
            'Hampshire & Isle of Wight',
            'Oxford',
            'Kent',
            'London',
            'Surrey',
            'Sussex',
            'Isle of Man/Ellan Vannin',
            'Guernsey',
            'Jersey']

bbc_link = ["https://www.bbc.co.uk/news/england/cumbria",                     
"https://www.bbc.co.uk/news/england/lancashire",                  
"https://www.bbc.co.uk/news/england/liverpool",                   
"https://www.bbc.co.uk/news/england/manchester",                  
"https://www.bbc.co.uk/news/england/tees",                        
"https://www.bbc.co.uk/news/england/tyne_and_wear",               
"https://www.bbc.co.uk/news/england/humberside",                  
"https://www.bbc.co.uk/news/england/leeds_and_west_yorkshire",    
"https://www.bbc.co.uk/news/england/lincolnshire",                
"https://www.bbc.co.uk/news/england/south_yorkshire",             
"https://www.bbc.co.uk/news/england/york_and_north_yorkshire",    
"https://www.bbc.co.uk/news/england/birmingham_and_black_country",
"https://www.bbc.co.uk/news/england/coventry_and_warwickshire",   
"https://www.bbc.co.uk/news/england/hereford_and_worcester",      
"https://www.bbc.co.uk/news/england/shropshire",                  
"https://www.bbc.co.uk/news/england/stoke_and_staffordshire",     
"https://www.bbc.co.uk/news/england/derby",                       
"https://www.bbc.co.uk/news/england/leicester",                   
"https://www.bbc.co.uk/news/england/northampton",                 
"https://www.bbc.co.uk/news/england/nottingham",                  
"https://www.bbc.co.uk/news/england/bristol",                     
"https://www.bbc.co.uk/news/england/cornwall",                    
"https://www.bbc.co.uk/news/england/devon",                       
"https://www.bbc.co.uk/news/england/gloucestershire",             
"https://www.bbc.co.uk/news/england/somerset",                    
"https://www.bbc.co.uk/news/england/wiltshire",                   
"https://www.bbc.co.uk/news/england/beds_bucks_and_herts",        
"https://www.bbc.co.uk/news/england/cambridgeshire",              
"https://www.bbc.co.uk/news/england/essex",                       
"https://www.bbc.co.uk/news/england/norfolk",                     
"https://www.bbc.co.uk/news/england/suffolk",                     
"https://www.bbc.co.uk/news/england/berkshire",                   
"https://www.bbc.co.uk/news/england/dorset",                      
"https://www.bbc.co.uk/news/england/hampshire",                   
"https://www.bbc.co.uk/news/england/oxford",                      
"https://www.bbc.co.uk/news/england/kent",                        
"https://www.bbc.co.uk/news/england/london",                      
"https://www.bbc.co.uk/news/england/surrey",                      
"https://www.bbc.co.uk/news/england/sussex",                      
"https://www.bbc.co.uk/news/world/europe/isle_of_man",            
"https://www.bbc.co.uk/news/england/guernsey",                    
"https://www.bbc.co.uk/news/england/jersey"]

bbc_dict = {'area': area,
            'bbc_link': bbc_link
           }
bbc_df = pd.DataFrame(data = bbc_dict)
bbc_df


# In[3]:

# function to extract the word after the last slash (localtion)
def extract_name(url):
    result = url.rsplit('/', 1)[-1]
    return result


# In[4]:

# extract localtion and use it to create the final BBC RSS links
bbc_df['location'] = bbc_df['bbc_link'].apply(extract_name)
bbc_df['rss_link'] = 'http://feeds.bbci.co.uk/news/england/' + bbc_df['location'].map(str) + '/rss.xml'
bbc_df


# In[5]:

### correcting broken URLs ####

broken_areas = ['Liverpool', 'Derby', 'Northampton', 'Isle of Man', "Guernsey", "Jersey"]

# Liverpool
bbc_df.bbc_link[bbc_df.area == 'Liverpool'] = 'https://www.bbc.co.uk/news/england/merseyside'
bbc_df.rss_link[bbc_df.area == 'Liverpool'] = 'http://feeds.bbci.co.uk/news/england/merseyside/rss.xml'

# Derby
bbc_df.bbc_link[bbc_df.area == 'Derby'] = 'https://www.bbc.co.uk/news/england/derbyshire'
bbc_df.rss_link[bbc_df.area == 'Derby'] = 'http://feeds.bbci.co.uk/news/england/derbyshire/rss.xml'

# Northampton
bbc_df.bbc_link[bbc_df.area == 'Northampton'] = 'https://www.bbc.co.uk/news/england/northamptonshire'
bbc_df.rss_link[bbc_df.area == 'Northampton'] = 'http://feeds.bbci.co.uk/news/england/northamptonshire/rss.xml'

# Isle of Man
bbc_df.bbc_link[bbc_df.area == 'Isle of Man/Ellan Vannin'] = 'https://www.bbc.co.uk/news/world/europe/isle_of_man'
bbc_df.rss_link[bbc_df.area == 'Isle of Man/Ellan Vannin'] = 'http://feeds.bbci.co.uk/news/world/europe/isle_of_man/rss.xml'

# Guernsey
bbc_df.bbc_link[bbc_df.area == 'Guernsey'] = 'https://www.bbc.co.uk/news/world/europe/guernsey'
bbc_df.rss_link[bbc_df.area == 'Guernsey'] = 'http://feeds.bbci.co.uk/news/world/europe/guernsey/rss.xml'

# Jersey
bbc_df.bbc_link[bbc_df.area == 'Jersey'] = 'https://www.bbc.co.uk/news/world/europe/jersey'
bbc_df.rss_link[bbc_df.area == 'Jersey'] = 'http://feeds.bbci.co.uk/news/world/europe/jersey/rss.xml'


bbc_df


# ## Connect to MySQL

# In[6]:

### define MySQL parameters
#Details required to connect to DB
host="nightingaledb.cjhl1bmdwdwk.us-east-1.rds.amazonaws.com"
port=3306
dbname="rss_feeds"
user="flozza"
password="zArp.9Fe#i"


# In[7]:

### connect to MySQL
connect_string = 'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4'.format(user,password,host,port,dbname)
engine = create_engine(connect_string, convert_unicode=True, echo=False)


# In[8]:

# view all available tables
sql_tbls = pd.read_sql('SELECT * FROM information_schema.tables', con = engine)
sql_tbls[sql_tbls['TABLE_SCHEMA'] == 'rss_feeds']


# ## getting archived RSS feeds URLs

# In[9]:

### getting archives
def get_archives(url):
    api_start = 'http://archive.org/wayback/available?url='
    cdx_uri = 'http://web.archive.org/cdx/search/cdx?url=' + url
    resp = get(cdx_uri)
    text = resp.text
    text = text.splitlines()
    return text

# split received text
def split_txt(text):
    split_items=[]
    for i in text:
        split_items.append(i.split(' '))
    return split_items

# save split_text as dataframe
def rss_to_df(split_text):
    df_test = pd.DataFrame(split_text)
    df_test.columns = ['rss_link', 'date', 'orig_link', 'format', 'status_code', 'code', 'number']
    df_test['final_link'] = 'http://web.archive.org/web/'+ df_test['date'].map(str) + '/' + df_test['orig_link'].map(str)
    return df_test


# # save archive links to MySQL

# In[10]:

### WRAP THIS UP INTO THE LOOP THAT WILL SAVE THE RESUTS TO MYSQL

#url = bbc_df['rss_link'][0]
for url in bbc_df['rss_link']:
    text = get_archives(url)
    if len(text) == 0:
        print("Empty archive at %s" %url)
    else:
        spl_text = split_txt(text)
        text_df = rss_to_df(spl_text)
        text_df.to_sql(con=engine, name='COMPLETE_archived_rss_urls', if_exists='append',index=False)

## below news branches don't have any internet archives!!


# In[11]:

# check if it saved it on MySQL - looks ok
sql_test = pd.read_sql('SELECT * FROM  COMPLETE_archived_rss_urls', con=engine)
sql_test.shape


# In[12]:

# check the sql table
sql_test.head()


# In[13]:

# extract location from the URL
def pick_location(rss_link):
    result = rss_link.split('/')[-2]
    return result


# In[14]:

# extract location from the URL and add it to the URL dataset
sql_test['location'] = sql_test['rss_link'].apply(pick_location)

# update the SQL table
sql_test.to_sql(con=engine, name='`COMPLETE_archived_rss_urls', if_exists='replace',index=False)


# In[15]:

sql_test.tail()


# # scrape the content of archived RSS feeds and save it in MySQL

# In[35]:

# function that scrapes the content of RSS feeds, adds location and saves the results on MySQL db
def get_rss(urls):
    for url in urls:
        posts = []
        location = pick_location(url)
        feed = feedparser.parse(url)
        for post in feed.entries:
            posts.append((post.title, post.link, post.summary, post.published, location))
        df = pd.DataFrame(posts, columns=['title', 'link', 'summary', 'date_published', 'location'])
        df.to_sql(con=engine, name='COMPLETE_archived_rss_data', if_exists='append',index=False)
        print(str(url) + ' Done')


# In[40]:

# ## testing get_rss function - it works!
# test_df = sql_test.sample(frac = 1.0).groupby('location').head(1)
# urls = test_df.final_link
# #test_df
# #urls
# get_rss(urls)


# In[ ]:

# scrape all the links and ave the results on MySQL
urls = sql_test.final_link
get_rss(urls)


# In[41]:

test_query = pd.read_sql('SELECT * FROM COMPLETE_archived_rss_data', con = engine)
test_query.shape


# In[21]:

# ### uploading county postcodes to MySQL ####
# # script with scraping the data can be found in kasia/kk-rss-feeds/002-fetching-EN-postcodes-per-county.R

# pc_tbl = feather.read_dataframe('20181015-EN-postcodes-per-county.feather')
# pc_tbl.to_sql(con=engine, name='county_postcodes', if_exists='replace',index=False)


# In[22]:

# test_sql = pd.read_sql('SELECT * FROM county_postcodes',  con=engine)
# test_sql.shape

