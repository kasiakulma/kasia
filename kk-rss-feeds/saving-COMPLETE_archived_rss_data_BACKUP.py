
# coding: utf-8

# In[ ]:

import pandas as pd
#from memento_client import MementoClient
#import urllib2
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import os, io, feedparser, json, datetime, requests, feather

from sqlalchemy import create_engine


# In[ ]:

sql_test = pd.read_sql('SELECT * FROM  COMPLETE_archived_rss_urls', con=engine)
sql_test.shape


# In[ ]:

def pick_location(rss_link):
    result = rss_link.split('/')[-2]
    return result

def get_rss(urls):
    for url in urls:
        posts = []
        location = pick_location(url)
        feed = feedparser.parse(url)
        for post in feed.entries:
            posts.append((post.title, post.link, post.summary, post.published, location))
        df = pd.DataFrame(posts, columns=['title', 'link', 'summary', 'date_published', 'location'])
        df.to_sql(con=engine, name='COMPLETE_archived_rss_data_BACKUP', if_exists='append',index=False)
        print(str(url) + ' Done')


# In[ ]:

# scrape all the links and ave the results on MySQL
urls = sql_test.final_link
get_rss(urls)


# In[ ]:

# check RSS feeds in MySQL
test_query = pd.read_sql('SELECT * FROM COMPLETE_archived_rss_data_BACKUP', con = engine)
test_query.shape

