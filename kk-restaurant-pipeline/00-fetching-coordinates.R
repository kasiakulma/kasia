#### packages ####

library(httr)
library(jsonlite)
library(dplyr)
library(stringr)
library(tidyr)
library(purrr)
library(readr)

source('functions.R')


#### import and clean test data ####
test_list <- read_csv("restaurant-test-list.csv")

glimpse(test_list)

# extract postcodes from address strings
test_list <- test_list %>% 
  mutate(post_code = str_extract(
    str_to_lower(address), "[a-ik-pr-uwyz]?[a-h,k-y][0-9]?[0-9a-hjkmnp-y][ ][0-9][abd-hjlnp-uw-z][abd-hjlnp-uw-z]")
  )

test_list$post_code

#### test get_coord() function ####

coord_test <- map_df(test_list$post_code, get_coord)
glimpse(coord_test)
summary(coord_test)

final_pc <- test_list %>% 
  left_join(coord_test, by = c("post_code" = "original_pc"))
