#### packages ####

library(httr)
library(jsonlite)
library(dplyr)
library(stringr)
library(tidyr)
library(purrr)
library(readr)
library(xml2)
library(rvest)
library(textclean)

#### source coordinates ####

source("functions.R")
source("00-fetching-coordinates.R")

glimpse(final_pc)


edited_address <- final_pc %>% 
  mutate(edited_address = str_trim(
    str_replace_all(
      final_pc$address, '^[:digit:]+,', '')
    )
  ) %>% 
  tidyr::separate(edited_address,
                  c("pre_street", "area", "rest"),
                  sep = ",") %>% 
 # select(original_address, pre_street, area, rest) %>% 
  as.data.frame() %>% 
  mutate(street = str_replace_all(str_trim(pre_street),'[:digit:]+[:punct:][:digit:]+[:space:]', ''),
         street = str_replace_all(street,'[:digit:]+[A-Za-z][:space:]', ''),
         street = str_trim(str_replace_all(street,'[:digit:]+[:space:]', '')),
         min_street = str_replace_all(str_to_lower(street), "st", ""),
         min_street = str_replace_all(str_to_lower(min_street), "rd", ""),
         min_street = str_replace_all(str_to_lower(min_street), "ave", ""),
         min_street = str_replace_all(str_to_lower(min_street), "rd", ""),
         min_street = str_replace_all(str_to_lower(min_street), "gate", ""),
         min_street = str_replace_all(str_to_lower(min_street), "sq", ""),
         min_street = str_trim(str_replace_all(str_to_lower(min_street), "ct", ""))
  )

glimpse(edited_address)
edited_address$min_street
edited_address[10, ]
#### zomato offers up to 1000 free API calls/day

key = "6fcd78fdb96c0be566fdd82298430f28"

glimpse(edited_address)



    

#### applying get_zomato() function to the whole sample ####
multi_test <- pmap(list(lat = edited_address$latitude,
                        long = edited_address$longitude,
                        name = edited_address$name,
                        street = edited_address$street,
                        postcode = edited_address$postcode),
                   get_zomato)


glimpse(multi_test)

### turning results into a clean data.frame
unnested_df <- map_df(multi_test, zomato_deep_unnest)
# str(unnested_df)
# summary(unnested_df)

# checking records with no zomato match 
# 10 records with no match - 7 genuinly missing from zomato, 3 are there
unnested_df %>% 
  filter(is.na(restaurant.name))


#### separating opening hours ####
str(multi_test)
multi_test[[1]]$opening_hours[[1]]

unnested_times_tbl <- map_df(multi_test, unnest_zomato_times) %>% 
  select(-opening_hours)
str(unnested_times_tbl)
unnested_times_tbl
