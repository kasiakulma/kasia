# -*- coding: utf-8 -*-
"""
Download all business data from the Food Hygiene Agency
"""

# TODO: Mostly error handling - currently needs babysitting due to sporadic failures

import requests
from pandas.io.json import json_normalize

pageSize = 2000 # Number of organisations to download at a time
maxPages = 500 # Max number of pages before stopping
startPage = 1 # Page to start from (use to continue from an error)

pickle_filename = 'D:/Data/Hygiene/all_ratings.pkl'
csv_filename    = 'D:/Data/Hygiene/all_ratings.tsv'
url_template = "http://ratings.food.gov.uk/search/en-gb/^/^/Alpha/{page:d}/{pageSize:d}/json"
request_headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}


def download_ratings(page, PageSize, url_template, request_headers):
    ''' Download all ratings from the Food Standards Agency API.
        Returns a dataframe '''
    # Construct a URL to download for this page
    vars = {'page':page, 'pageSize':pageSize}
    url = url_template.format(**vars)
    print(url)

    # Actually download the data
    r = requests.get(url, headers=request_headers, timeout=60)
    #print(r.text)
    
    # Interpret the download to turn it into a dataframe of organisations
    json_full = r.json()
    json_collection = json_full['FHRSEstablishment']['EstablishmentCollection']
    if len(json_collection) <=2: # stop if the download is empty
        return None
    json_content = json_collection['EstablishmentDetail']
    return json_normalize(json_content)
     
def append_to_dataframe(df, df_all):
    ''' Appends df to df_all, creating from scratch if it doesn't exist '''
    if df_all is None: # Add this page to the full dataset
        df_all = df.copy()
    else:
        df_all = df_all.append(df, ignore_index=True)


if 'df_all' not in locals(): # Initialise df_all if it's not there already
    df_all = None

for page in range(startPage, maxPages+1):
    df = download_ratings(page, pageSize, url_template, request_headers)
    append_to_dataframe(df, df_all)
        
    if df is None: # If no more data, then finish
        break
 
df_all.to_pickle(pickle_filename)
df_all.to_csv(csv_filename, sep='\t')

